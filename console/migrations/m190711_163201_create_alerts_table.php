<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%alerts}}`.
 */
class m190711_163201_create_alerts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%alerts}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%alerts}}');
    }
}
