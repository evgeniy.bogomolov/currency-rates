<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%metrika}}`.
 */
class m190711_114751_create_metrika_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%metrika}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime(),
            'currency_id' => $this->integer(),
            'currency_rate' => $this->float()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%metrika}}');
    }
}
