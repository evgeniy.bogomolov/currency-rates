<?php

use yii\db\Migration;

/**
 * Class m190711_163435_add_currency_id_column_to_alerts
 */
class m190711_163435_add_currency_id_column_to_alerts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%alerts}}', 'currency_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190711_163435_add_currency_id_column_to_alerts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190711_163435_add_currency_id_column_to_alerts cannot be reverted.\n";

        return false;
    }
    */
}
