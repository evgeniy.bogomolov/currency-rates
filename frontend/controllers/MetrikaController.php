<?php
namespace frontend\controllers;

use frontend\models\Log;
use frontend\models\Currency;
use frontend\models\Metrika;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class MetrikaController extends Controller
{
    /**
     * Добавляет запись курсов валют в таблицу раз в день по крону
     */
    public function actionAdd()
    {
        Currency::addCurrencyRatesToMetrika();
    }

    /**
     * Проверяет лог доступности курсов валют
     */
    public function actionCheck()
    {
        $log = Log::find()->orderBy('date desc')->one();
        if(!$log->status) {
            date_default_timezone_set('Europe/Samara');
            $current_date = date("Y-m-d H:i:s");

            $minutes_diff = (strtotime($current_date) - strtotime($log->date)) / 60;

            echo $minutes_diff;

            if($minutes_diff > 1 && $minutes_diff < 3) { //на всякий случай сделал 2 дополнительные минуты
                Currency::addCurrencyRatesToMetrika();
            }
            if($minutes_diff > 29 && $minutes_diff < 32) {
                Currency::addCurrencyRatesToMetrika();
            }
            if($minutes_diff > 59 && $minutes_diff < 62) {
                Currency::addCurrencyRatesToMetrika();
            }
            if($minutes_diff > 359 && $minutes_diff < 362) {
                Currency::addCurrencyRatesToMetrika();
            }
        }
    }
}