<?php

namespace frontend\models;

use app\models\Alert;
use Yii;

/**
 * This is the model class for table "currencies".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Currency extends \yii\db\ActiveRecord
{
    public $rate;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
        ];
    }

    /**
     * Принимает массив валют, отдает его же + текущий курс для каждой валюты
     */
    static function currencyRates($currencies){
        $xml_url = "http://www.cbr.ru/scripts/XML_daily.asp";
        $Headers = @get_headers($xml_url);

        if(preg_match("|200|", $Headers[0])) {
            $xml = simplexml_load_file($xml_url);
            foreach ($currencies as $currency) {
                $rate = (string)$xml->xpath('/ValCurs/Valute[@ID="'.$currency->code.'"]/Value')[0];
                $rate = floatval(str_replace(",",".",$rate));
                $currency->rate = $rate;
            }

        } else {
            return false;
        }

        return $currencies;
    }

    /**
     * Проверяет доступность XML файла с курсами. Добавляет их в метрику и в лог.
     */
    static function addCurrencyRatesToMetrika() {
        $log = new Log();

        if($currencies = Currency::currencyRates(Currency::find()->all())) {
            foreach ($currencies as $currency) {
                $metrika = new Metrika();

                $metrika->currency_id = $currency->id;
                $metrika->currency_rate = $currency->rate;

                $last_rate = Metrika::find()->where(["currency_id" => $currency->id])->orderBy('date desc')->one()->currency_rate;
                $rate_diff =  $last_rate - $metrika->currency_rate;

                if($rate_diff > 2) {
                    $alert = new Alert();

                    $alert->currency_id = $currency->id;
                    $alert->save();
                }

                $metrika->save();
            }

            $log->status = 1;
        }else {
            $log->status = 0;
        }

        $log->save();
    }
}
