<?php

/* @var $this yii\web\View */
use frontend\models\Currency;
use miloschuman\highcharts\Highcharts;
$this->title = 'Курс валют';
?>
<div class="site-index">

    <div class="body-content">
        <?php

        if ($alerts):
                foreach ($alerts as $alert):
                    ?>
                    <div id="w21" class="box box-warning box-solid">
                        <div class="box-header">
                            <h3 class="box-title">Курс упал!</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button></div></div><div class="box-body">
                            <p>Курс валюты <?= $alert ?> за последние сутки упал более, чем на 2 рубля!</p>

                        </div>
                    </div>
                    <?php
                endforeach;
            endif;

        echo Highcharts::widget([
            'options' => [
                'title' => [
                    'text' => 'Общая статистика'
                ],
                'xAxis' => [
                    'type'=> 'datetime',
                    'title' => [
                        'text' => 'Время'
                    ]
                ],
                'yAxis' => [
                    'title' => [
                        'text' => 'Средний показатель'
                    ]
                ],
                'series' => $data,
            ]
        ]);
        ?>
    </div>
</div>
